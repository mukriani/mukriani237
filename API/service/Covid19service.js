module.exports=exports=(server, pool)=>{

    server.post('/api/Covidpost', (req, res) => {
        console.log(req.body);
        const { Tanggal, Positif, Sembuh, Meninggal
        } = req.body;
        var query =`INSERT INTO public."Covid19"("Active",Tanggal" ,"Positif", "Sembuh", "Meninggal")			
            VALUES (false, '${Tanggal}', '${Positif}', '${Sembuh}','${Meninggal}');

            `;  pool.query(query, (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: `Data ${Tanggal} berhasil disimpan`
                    })
                }
            }
        )
        console.log(query);
    });

   
    server.get('/api/getCovid', (req, res) => {
        pool.query(`SELECT "id", "Tanggal", "Sembuh", "Positif", "Meninggal" FROM public."Covid19"`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });

            }
        })

    });

    server.get('/api/Covid_get/:id', (req, res) => {
        const id = req.params.id;

        pool.query(`Select * from public."Covid19" where "id" = ${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    })
                }
                else {
                    res.send(400, {
                        success: false,
                        result: `Data not Found`
                    })
                }
            }
        })
    });
    server.put('/api/updateCovid/:id', (req, res) => {
        const id = req.params.id;
        const{Tanggal, Positif, Sembuh, Meninggal}=req.body;

        var query= `UPDATE public."Covid19" 
        SET 
        "Tanggal"='${Tanggal}',
        "Positif"='${Positif}', 
        "Sembuh"='${Sembuh}', 
        "Meninggal"='${Meninggal}'
        WHERE "id"=${id}
         `;  pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: `Data ${Tanggal} Berhasil diubah`
                });
                }
               
        })
        console.log(query);
    });
    
     server.put('/api/deleteCovid/:id', (req, res) => {
        const id = req.params.id;
        const{Tanggal}= req.body;
        console.log(`update public."Covid19" SET "Active"='true' 
        where "id"='${id}';`)
        pool.query(`update public."Covid19" SET "Active"='true' 
        where "id"='${id}';`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: `Data ${Tanggal} Berhasil dihapus`
                });
               
            }
        })
    });
}
