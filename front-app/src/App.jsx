import React from 'react';
import logo from './logo.svg';
import Content from './Layout/content';
import './App.css';
import Collection from './Collection';
import Sidebar from './Layout/sidebar';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            

            <Content />
          </Switch>

        </BrowserRouter>
      </div>
    )
  }
}
