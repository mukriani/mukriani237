import React from 'react';
import { config } from '../config/config';

export default class Header extends React.Component {
    render() {
        return (
            <header class="main-header">
            <a href="index2.html" class="logo">
              <span class="logo-mini"><b>M</b>A</span>
              <span class="logo-lg"><b>Mukri</b>ani</span>
                </a>
                <nav class="navbar navbar-static-top">

                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    </nav>
                    <div class="navbar navbar-static-top">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu">
                                <h2>Hi{localStorage.getItem(config.username)}</h2>
                            </li>
                        </ul>
                    </div>
                    {/* <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu">
                                <h2>Hallo, {localStorage.getItem(config.username)} </h2> */}

               
            </header>

        )
    }
}