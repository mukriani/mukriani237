import React from 'react';
import Header from '../Layout/Header';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Sidebar from '../Layout/sidebar';
import Collection from '../Collection';


export default class content extends React.Component{
    render(){
        return(
            <div className="wrapper">
                {/* <Header/> */}
                <Sidebar/>
                <div className="content-wrapper">
                    <section className="content">
                        <Switch>

                            <Route exact path='/Collection' component={Collection}/>
                        </Switch>
                    </section>
                </div>
            </div>
        )
    }
}