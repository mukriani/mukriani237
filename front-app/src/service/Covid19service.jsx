import axios from 'axios';
import { config } from '../config/config';

const Covid19service = {
    getAll: () => {
        const result = axios.get(config.apiUrl + '/getCovid')
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/Covidpost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
                    return result;
    },
    updateCovid: (item) => {
        const result = axios.put(config.apiUrl + '/updateCovid/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deleteCovid/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;

    },
    getCovidbyid: (id) => {
        const result = axios.get(config.apiUrl + '/Covid_get/' + id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error

                }
            });
        return result;
    }
}


export default Covid19service;