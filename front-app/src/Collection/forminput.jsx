import React from 'react';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

class forminput extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedOption: {}
        }
    }

    render() {
        const { hendleopen,
            changeHendler,
            handlerEdit,
            onSave,
            CovidModel,
            hidden,
            title,
            openmodalEdit,
            cancelHendler,
            mode
        } = this.props;

        return (
            <div>
                <Button variant="primary" onClick={hendleopen}>Create</Button><br></br>
                <Modal show={openmodalEdit} style={{ opacity: 1 }}>
                    <Modal.Header style={{ backgroundColor: "Orange" }}>
                        <Modal.Title>{title}</Modal.Title>

                    </Modal.Header>
                    <Modal.Body>
                        <div className='form-inside-input'>
                            <label className="form-label">Tanggal</label>
                            <input type="date" Id="Tanggal" className="form-control" value={CovidModel.Tanggal} onChange={changeHendler("Tanggal")} /><br></br>
                            <label className="form-label">Positif</label>
                            <input type="text" Id="Positif" className="form-control" value={CovidModel.Positif} onChange={changeHendler("Positif")} /><br></br>
                            <label className="form-label">Sembuh</label>
                            <input type="text" Id="Sembuh" className="form-control" value={CovidModel.Sembuh} onChange={changeHendler("Sembuh")} /><br></br>
                            <label className="form-label">Meninggal</label>
                            <input type="text" Id="Meninggal" className="form-control" value={CovidModel.Meninggal} onChange={changeHendler("Meninggal")} /><br></br>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="modal-Footer">
                            <button type="button" className="btn btn-primary" onClick={onSave}>Save changes</button>&emsp;
                     <button type="button" className="btn btn-secondary" onClick={cancelHendler}>close</button>
                        </div>
                    </Modal.Footer>
                </Modal>

                {/* <button>Cancel</button>
                <button onClick={onSave}>Save</button> */}
            </div>


        )
    }
}
export default forminput