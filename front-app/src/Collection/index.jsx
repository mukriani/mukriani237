import React from 'react';
import FormInput from './forminput';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import Covid19service from '../service/Covid19service';
import {ModalBody,ModalFooter,ModalHeader, Container, Row, Col,Card, CardTitle, CardText, Collapse } from 'reactstrap'
import { Link } from 'react-router-dom';

class Covid19 extends React.Component {

    CovidModel = {
        id: 0,
        Tanggal: '',
        Positif:'',
        Sembuh: '',
        Meninggal: '',
        Active: false

    }
    constructor() {
        super();
        this.state = {
            ListCovid: [],
            CovidModel: this.CovidModel,
            hidden: true,
            mode: "",
            openmodalEdit: false,
            openmodalDelete: false,
            fields: {},
            errors: {}
        }
    }
    componentDidMount() {
        this.loadList();
    }

    loadList = async () => {
        const respon = await Covid19service.getAll();
        if (respon.success) {
            this.setState({
                ListCovid: respon.result
            })
        }
    }
    hendleopen = () => {
        // this.getOptionKategori();
        this.setState({
            hidden: false,
            openmodalEdit: true,
            CovidModel: this.CovidModel,
            mode: 'Create'
        })
    }
    hendlerEdit = async (id) => {
        const respon = await Covid19service.getCovidbyid(id);
        // this.getOptionKategori();
        if (respon.success) {
            this.setState({
                hidden: false,
                CovidModel: respon.result,
                mode: 'Edit',
                openmodalEdit: true


            })
        }
        else {
            alert('error' + respon.result);
        }
    }
    cancelHendler = async () => {
        // const respon = await BarangService.getAll();
        // if (respon.success) {
        this.setState({
            // BarangModel: respon.result,
            openmodalEdit: false

        });

    }
    hendledelete = async (id) => {
        const respon = await Covid19service.getCovidbyid(id);
        if (respon.success) {
            this.setState({
                CovidModel: respon.result,
                openmodalDelete: true

            });
        }
        else {
            alert('error' + respon.result);
        }
    }
    changeHendler = name => ({ target: { value } }) => {
        this.setState({
            CovidModel: {
                ...this.state.CovidModel,
                [name]: value
            }
        })
    }
    checkedHendler = name => ({ target: { checked } }) => {
        this.setState({
            CovidModel: {
                ...this.state.CovidModel,
                [name]: checked
            }
        })
    }
   
    sureDelete = async (item) => {
        const { CovidModel } = this.state;
        const respons = await Covid19service.delete(CovidModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList();
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDelete: false
        });
    }

   
    onSave = async () => {
        const { CovidModel, mode } = this.state;
            if (mode === 'Create') {
                const respons = await Covid19service.post(CovidModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList();
                    this.setState({
                        hidden: true,

                    });
                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
                
            } else {
                const respon = await Covid19service.updateCovid(CovidModel);
                if (respon.success) {
                    alert('Success : ' + respon.result)
                    this.loadList();
                }
                else {
                    alert('Error : ' + respon.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
            }
        }
    


    render() {
        const { ListCovid, CovidModel, hidden, openmodalDelete, openmodalEdit, title, mode, errors } = this.state;
return (
    <div>
        <h5>COVID-19</h5>
        <FormInput
            hendleopen={this.hendleopen}
            changeHendler={this.changeHendler}
            checkedHendler={this.checkedHendler}
            onSave={this.onSave}
            CovidModel={CovidModel}
            hidden={hidden}
            title={mode}
            openmodalEdit={openmodalEdit}
            cancelHendler={this.cancelHendler}
            hendlerEdit={this.hendlerEdit}
            selectedHandler={this.selectedHandler}
            mode={mode}
        />

        <Table stripped bordered hover>
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Positif</th>
                    <th>Meninggal</th>
                    <th>Sembuh</th>
                </tr>
            </thead>
            <tbody>
                {
                    ListCovid.map(co => {
                        return (
                            <tr key={co.id}>
                                <td>{co.Tanggal}</td>
                                <td>{co.Positif}</td>
                                <td>{co.Sembuh}</td>
                                <td>{co.Meninggal}</td>

                                {/* <input> type="checkbox" checked={brg.Active></input> */}
                                <td><Button variant='primary' onClick={() => this.hendlerEdit(co.id)}>Edit</Button>&emsp;
                                   </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </Table>
        <Modal show={openmodalDelete}>
            <Modal.Body>mau delete {CovidModel.Tanggal} ? </Modal.Body>
            <Modal.Footer><Button variant="danger" onClick={() => this.sureDelete(CovidModel)}>Tentu</Button></Modal.Footer>
        </Modal>

    </div>
)
    }
}export default Covid19;